#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QRadioButton>
#include <QGroupBox>
#include <QFileDialog>
#include <QLabel>
#include <QDebug>
#include "crypt.h"
#include <iostream>
#include <fstream>
#include <sstream>


class MainWindow : public QMainWindow
{
    Q_OBJECT
    Crypt *CryptC;
    QPushButton *inputFileButton, *outputFileButton, *keyFileButton, *startPushButton;
    QRadioButton *encryptRadioButton, *decryptRadioButton;
    QGroupBox *groupBox();
    QLabel *inputFileNameLabel, *outFileNameLabel, *keyFileNameLabel;
    QString inputFileName, outputFileName, keyFileName;
    const QSize btnSize = QSize(100, 30);
    void createMainLayout();
    void processFiles();

signals:
    void passData(const std::string text, const std::string key, bool mode);

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void receiveData(std::string data);
};

#endif // MAINWINDOW_H
