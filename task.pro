#-------------------------------------------------
#
# Project created by QtCreator 2019-07-06T13:05:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = task
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    crypt.cpp \
    Gost/28147_14.c \
    Gost/block_chipher.c \
    Gost/28147_89.c

HEADERS += \
        mainwindow.h \
    crypt.h \
    Gost/28147_14.h \
    Gost/callback_print.h \
    Gost/dll_import.h \
    Gost/block_chipher.h \
    Gost/28147_14.h \
    Gost/28147_89.h

#INCLUDEPATH += C:\OpenSSL-Win64\include
#DEPENDPATH += C:\OpenSSL-Win64\include
#LIBS += -LC:\OpenSSL-Win64\lib -llibeay32
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=
