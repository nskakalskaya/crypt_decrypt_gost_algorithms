#include "crypt.h"
#include <QFile>
#include <random>
#include <climits>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <functional>

using random_bytes_engine = std::independent_bits_engine<std::default_random_engine, CHAR_BIT, unsigned char>;


void Crypt::encrypt(std::string plainText, std::string key)
{
    std::string iv;
    iv.resize(ivLen);
    random_bytes_engine rbe;
    rbe.seed(static_cast<long unsigned int>(time(0)));
    std::generate(begin(iv), end(iv), std::ref(rbe));

    unsigned char ctx[kCfb14ContextLen];
    const int nBytes = plainText.size();
    std::string cipherStr;
    cipherStr.resize(nBytes);

    if (init_cfb_14((unsigned char *)key.c_str(), ctx, blockSize, (unsigned char *)iv.c_str(), ivLen))
    {
        QMessageBox msgBox;
        msgBox.setText("Unable to encrypt file");
        msgBox.exec();
        return;
    }

    if (encrypt_cfb(ctx, (unsigned char *)plainText.c_str(), (unsigned char *)cipherStr.c_str(), nBytes))
    {
        QMessageBox msgBox;
        msgBox.setText("Unable to encrypt file");
        msgBox.exec();
        return;
    }
    else
    {
        free_cfb(ctx);
        qDebug() << "CIPHER" << QString::fromStdString(cipherStr) << " IV " << QString::fromStdString(iv);
        emit sendData(iv + cipherStr);
        QMessageBox msgBox;
        msgBox.setText("Done!");
        msgBox.exec();
    }
}

void Crypt::decrypt(std::string cipherText, std::string key)
{
    const int nBytes = cipherText.size();
    unsigned char ctx[kCfb14ContextLen];
    std::string decryptedStr;
    decryptedStr.resize(nBytes-ivLen);

    std::string iv = cipherText.substr(0, ivLen);
    cipherText.erase(0, ivLen);

    if (init_cfb_14((unsigned char *)key.c_str(), ctx, blockSize, (unsigned char *)iv.c_str(), ivLen))
    {
         QMessageBox msgBox;
         msgBox.setText("Unable to decrypt file");
         msgBox.exec();
         return ;
    }
    if (decrypt_cfb(ctx, (unsigned char *)cipherText.c_str(), (unsigned char *)decryptedStr.c_str(), nBytes-ivLen))
    {
        QMessageBox msgBox;
        msgBox.setText("Unable to decrypt file");
        msgBox.exec();
        return ;
    }
    else
    {
        free_cfb(ctx);
        qDebug() << QString::fromStdString(decryptedStr);
        emit sendData(decryptedStr);
        QMessageBox msgBox;
        msgBox.setText("Done!");
        msgBox.exec();
    }
}

Crypt::Crypt(QObject *parent) : QObject(parent)
{

}

void Crypt::getData(std::string text, std::string key, bool mode)
{
   if (key.size() < keyLen)
      key.insert(key.end(), keyLen - key.size(), '0');
   if (mode)
      encrypt(text, key);
   else
   {
      if (text.size() < ivLen)
      {
           QMessageBox msgBox;
           msgBox.setText("Unable to decrypt");
           msgBox.setInformativeText("File has been encrypted with error.");
           msgBox.exec();
      }
      else
          decrypt(text, key);
   }
   return;
}
