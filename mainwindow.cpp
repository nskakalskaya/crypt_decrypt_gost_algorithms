#include "mainwindow.h"

void MainWindow::MainWindow::createMainLayout()
{
    QHBoxLayout *bottomHorizontalLayout = new QHBoxLayout;
    inputFileButton = new QPushButton("Input file", this);
    outputFileButton = new QPushButton("Output file", this);
    keyFileButton = new QPushButton("Key file", this);
    inputFileButton->setFixedSize(btnSize);
    outputFileButton->setFixedSize(QSize(100, 30));
    keyFileButton->setFixedSize(QSize(100, 30));
    inputFileNameLabel = new QLabel(this);
    outFileNameLabel = new QLabel(this);
    keyFileNameLabel = new QLabel(this);
    startPushButton = new QPushButton("Start", this);
    startPushButton->setEnabled(false);
    startPushButton->setFixedSize(QSize(100, 30));
    encryptRadioButton = new QRadioButton("Encrypt", this);
    encryptRadioButton->setChecked(true);
    decryptRadioButton = new QRadioButton("Decrypt", this);
    bottomHorizontalLayout->addWidget(groupBox(), 0, Qt::AlignCenter);
    QVBoxLayout *mainVerticalLayout;
    mainVerticalLayout = new QVBoxLayout;
    mainVerticalLayout->addLayout(bottomHorizontalLayout);
    QWidget *mainWidget = new QWidget(this);
    mainWidget->setLayout(mainVerticalLayout);
    setCentralWidget(mainWidget);

}

void MainWindow::processFiles()
{
    std::ifstream infile(inputFileName.toStdString(), std::ios_base::in);
    std::ifstream keyfile(keyFileName.toStdString(), std::ios_base::in);
    if (!infile.is_open() || !keyfile.is_open())
    {
        QMessageBox msgBox;
        msgBox.setText("Error while opening file.");
        msgBox.exec();
        return;
    }
    else
    {
      std::stringstream ssText, ssKey;
      ssText << infile.rdbuf();
      ssKey << keyfile.rdbuf();
      emit passData(ssText.str(), ssKey.str(), encryptRadioButton->isChecked());
    }

}

QGroupBox *MainWindow::groupBox()
{
    QGroupBox *groupBox = new QGroupBox(tr("Encrypt/Decrypt"));
    QGridLayout *gridTop = new QGridLayout;
    gridTop->addWidget(inputFileButton, 0, 0);
    gridTop->addWidget(inputFileNameLabel, 0, 1);
    gridTop->addWidget(outputFileButton, 1, 0);
    gridTop->addWidget(outFileNameLabel, 1, 1);
    gridTop->addWidget(keyFileButton, 2, 0);
    gridTop->addWidget(keyFileNameLabel, 2, 1);
    QHBoxLayout *bottom = new QHBoxLayout;
    QVBoxLayout *layout = new QVBoxLayout;
    QGridLayout *grid = new QGridLayout;
    bottom->addStretch(1);
    bottom->addWidget(encryptRadioButton);
    bottom->addStretch(1);
    bottom->addWidget(decryptRadioButton);
    bottom->addStretch(1);
    layout->addWidget(startPushButton, 0, Qt::AlignCenter);
    grid->addLayout(gridTop, 0, 0);
    grid->addLayout(bottom, 1, 0);
    grid->addLayout(layout, 2, 0);
    groupBox->setLayout(grid);
    groupBox->setStyleSheet("QGroupBox { font-weight: bold; } ");
    return groupBox;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setMinimumHeight(400);
    setMinimumWidth(250);
    createMainLayout();
    CryptC = new Crypt(this);
    connect(inputFileButton, &QPushButton::clicked, this, [=]{
            QFileDialog *fd = new QFileDialog();
            QString fileName = fd->getOpenFileName();
            if (!fileName.isEmpty())
            {
                QFileInfo fi(fileName);
                inputFileNameLabel->setText(fi.fileName());
                inputFileName = fileName;
                if (!inputFileName.isEmpty() && !outputFileName.isEmpty() && !keyFileName.isEmpty())
                    startPushButton->setEnabled(true);
            }
        });
    connect(outputFileButton, &QPushButton::clicked, this, [=]{
            QFileDialog *fd = new QFileDialog();
            QString fileName = fd->getOpenFileName();
            if (!fileName.isEmpty())
            {
                QFileInfo fi(fileName);
                outFileNameLabel->setText(fi.fileName());
                outputFileName = fileName;
                if (!inputFileName.isEmpty() && !outputFileName.isEmpty() && !keyFileName.isEmpty())
                    startPushButton->setEnabled(true);
            }
        });
    connect(keyFileButton, &QPushButton::clicked, this, [=]{
            QFileDialog *fd = new QFileDialog();
            QString fileName = fd->getOpenFileName();
            if (!fileName.isEmpty())
            {
                QFileInfo fi(fileName);
                keyFileNameLabel->setText(fi.fileName());
                keyFileName = fileName;
                if (!inputFileName.isEmpty() && !outputFileName.isEmpty() && !keyFileName.isEmpty())
                    startPushButton->setEnabled(true);
            }
        });
    connect(startPushButton, &QPushButton::clicked, this, &MainWindow::processFiles);
    connect(this, &MainWindow::passData, CryptC, &Crypt::getData);
    connect(CryptC, &Crypt::sendData, this, &MainWindow::receiveData);
}

MainWindow::~MainWindow()
{

}

void MainWindow::receiveData(std::string data)
{
    std::ofstream ofs(outputFileName.toStdString(), std::ofstream::out | std::ios_base::trunc);
    if (!ofs.is_open())
    {
        QMessageBox msgBox;
        msgBox.setText("Error while opening out file.");
        msgBox.exec();
    }
    else
    {
        ofs << data;
        ofs.close();
    }
    return;
}
