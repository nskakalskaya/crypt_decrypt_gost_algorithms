#ifndef CRYPT_H
#define CRYPT_H

#include <QObject>
#include <QDebug>
#include <QMessageBox>
#include <ctime>
#include "Gost/block_chipher.h"


class Crypt : public QObject
{
    Q_OBJECT
    void encrypt(std::string plainText, std::string key);
    void decrypt(std::string cipherText, std::string key);
    const size_t ivLen = 32;
    const size_t blockSize = 16;
    const size_t keyLen = 32;
public:
    explicit Crypt(QObject *parent = nullptr);

signals:
    void sendData(std::string data);
public slots:
    void getData(std::string text, std::string key, bool mode);
};

#endif // CRYPT_H
